﻿using System;
using Xamarin.Forms;
using SQLite.Net;
using System.IO;

//[assembly: Dependency(typeof(SQLite_Android))]
namespace MobileClothingAdvisor.Droid
{
	public class SQLite_Android : ISQLite
	{
		public SQLite_Android ()
		{
			
		}

		public SQLiteConnection GetConnection ()
		{
			string databaseFileName = "AdvisorSqLite.db3";

			string documentsFolder = Environment.GetFolderPath (Environment.SpecialFolder.Personal);

			var platform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid ();

			var path = Path.Combine (documentsFolder, databaseFileName);

			var connection = new SQLiteConnection (platform, path);

			return connection;
		}
	}
}

