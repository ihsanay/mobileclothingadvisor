﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ConsumeWS.JemiWS;
using System.IO;
using System.Windows.Media.Imaging;


namespace ConsumeWS
{
    public partial class Form1 : Form
    {
        private MyWebSSoapClient ws = new MyWebSSoapClient();

        //private System.Windows.Forms.OpenFileDialog openFileDialog1;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            label1.Text = ws.add((int)numericUpDown1.Value, (int)numericUpDown2.Value).ToString();
        }

        private void btnMul_Click(object sender, EventArgs e)
        {
            label1.Text = ws.mul((int)numericUpDown1.Value, (int)numericUpDown2.Value).ToString();
            
        }

        private void btnPhoto_Click(object sender, EventArgs e)
        {

        }

        /*
        private void btnPhoto_Click(object sender, EventArgs e)
        {
            //BitmapImage için, (System.Windows.Media.Imaging;) Projeye sağ tıkla, Add Reference de, .Net sekmesinde "PresentationCore" ekle.
            Stream stream = (Stream)openDialog.File.OpenRead();
            byte[] bytes = new byte[stream.Length];
            stream.Read(bytes, 0, (int)stream.Length);
            BitmapImage bmi = new BitmapImage();
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                
                bmi.SetSource(ms);
                newRow.Thumbnail = bmi;
            }
        }
        */
    }
}
