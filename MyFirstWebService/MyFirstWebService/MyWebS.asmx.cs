﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
//using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.Util;
using Emgu.CV.CvEnum;
using System.Reflection;
using System.Configuration;
using System.IO;
using System.Xml; //renk adı algılamak için


namespace MyFirstWebService
{
    /// <summary>
    /// Summary description for MyWebS
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MyWebS : System.Web.Services.WebService
    {
        /*
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }
        */

        [WebMethod(Description="add two number")]
        public int add(int nbr, int nbr1)
        {
            return nbr + nbr1;
        }


        [WebMethod(Description = "add two number")]
        public int lol(int nbr, int nbr1)
        {
            return nbr + nbr1;
        }

        [WebMethod(Description = "multiple two number")]
        public int mul(int nbr, int nbr1)
        {
            return nbr * nbr1;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////

        [WebMethod(Description = "Yuz Tanima")]
        public int mul3(int nbr, int nbr1)
        {
           // DetectFaces();
            return 1;
            //return nbr * nbr1;
        }

        [WebMethod(Description = "UploadFile")]
        public string UploadFile(byte[] f, string fileName)
        {
            // the byte array argument contains the content of the file
            // the string argument contains the name and extension
            // of the file passed in the byte array
            try
            {
                // instance a memory stream and pass the
                // byte array to its constructor
                MemoryStream ms = new MemoryStream(f);

                // instance a filestream pointing to the
                // storage folder, use the original file name
                // to name the resulting file
                FileStream fs = new FileStream(System.Web.Hosting.HostingEnvironment.MapPath
                            ("~/TransientStorage/") + fileName, FileMode.Create);

                // write the memory stream containing the original
                // file as a byte array to the filestream
                ms.WriteTo(fs);

                // clean up
                ms.Close();
                fs.Close();
                fs.Dispose();

                // return OK if we made it this far
                return "OK";
            }
            catch (Exception ex)
            {
                // return the error message if the operation fails
                return ex.Message.ToString();
            }
        }

        [WebMethod(Description ="Get image content")]
        public byte[] GetImageFile(string fileName)
        {
            if (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/TransientStorage/") + fileName))
            {
                return System.IO.File.ReadAllBytes(System.Web.Hosting.HostingEnvironment.MapPath("~/TransientStorage/") + fileName);
            }
            else
            {
                return new byte[] { 0 };
            }
        }



        //////////////
        public static List<Color> TenMostUsedColors { get; private set; }
        public static List<int> TenMostUsedColorIncidences { get; private set; }

        public static Color MostUsedColor { get; private set; }
        public static int MostUsedColorIncidence { get; private set; }

        private static int pixelColor;

        private static Dictionary<int, int> dctColorIncidence;


        ///////////////////////

        //declaring global variables
        private Capture capture;        //takes images from camera as image frames
        private bool captureInProgress; 
        private HaarCascade haar; //viola-jones classifier (detector)

        Image<Bgr, Byte> ImageFrame;

        Bitmap[] ExtFaces;

        int faceNo = 0;


        public static Color getDominantColor(Bitmap bmp)
        {
            //Used for tally
            int r = 0;
            int g = 0;
            int b = 0;

            int total = 0;

            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    Color clr = bmp.GetPixel(x, y);

                    r += clr.R;
                    g += clr.G;
                    b += clr.B;

                    total++;
                }
            }

            //Calculate average
            r /= total;
            g /= total;
            b /= total;
            

            //MessageBox.Show(Convert.ToString(r) + "  " + Convert.ToString(g) + "  " + Convert.ToString(b));

            return Color.FromArgb(r, g, b);
            //return r;
            //return Color.FromArgb(r, g, b);
        }
        [WebMethod]
        public string DetectFaces(byte[] image)
        {
            
            MemoryStream ms = new MemoryStream(image);
            Image InputImg = Image.FromStream(ms);



            /*
            string filePath = ConfigurationManager.AppSettings.Get("ImageUploadPath");

            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            filePath = filePath + "\\" + picture.FileName + "." + picture.FileType;

            if (picture.FileName != string.Empty)
            {
                fileStream = File.Open(filePath, FileMode.Create);
                writer = new BinaryWriter(fileStream);
                writer.Write(picture.FileStream);
            }
            */


           haar = new HaarCascade(Server.MapPath("haarcascade_frontalface_alt_tree.xml")); 
            //byte to image 

            //fotoyu otomatik olarak alıyor
            //input image dışardan geldiği için
           //Image InputImg = Image.FromFile(Server.MapPath("AliYasak.jpg")); //input image dışardan geldiği için hesaba katmadık
            Image<Bgr, byte> ImageFrame = new Image<Bgr, byte>(new Bitmap(InputImg));
            //CamImageBox.Image = ImageFrame;



            Image<Gray, byte> grayframe = ImageFrame.Convert<Gray, byte>();
            

            //her yüz için dikdörtgen çizdiyoruz.
            //Haar_det_type için Using Emgu.CV.CvEnum; ekledik
            var faces =
                        grayframe.DetectHaarCascade(haar, 1.2, 2, HAAR_DETECTION_TYPE.DO_CANNY_PRUNING,
                        new Size(25, 25))[0];

            if (faces.Length > 0)
            {
                //MessageBox.Show("Total faces detected: " + faces.Length.ToString());
                //lblBilgi.Text = "Bilgi: Toplam algılanan yüz: " + faces.Length.ToString();
                Bitmap BmpInput = grayframe.ToBitmap();
                Bitmap ExtractedFace; //boş şimdilik
                Graphics FaceCanvas;

                ExtFaces = new Bitmap[faces.Length];
                


                foreach (var face in faces)
                {
                    int ilkDeger;
                    int ikinciDeger;

                    //ilkDeger = Convert.ToInt32(textBox1.Text);
                    //ikinciDeger = Convert.ToInt32(textBox2.Text);


                    ImageFrame.Draw(face.rect, new Bgr(Color.Green), 3);
                    ExtractedFace = new Bitmap(face.rect.Width, face.rect.Height);

                    FaceCanvas = Graphics.FromImage(ExtractedFace);

                    //FaceCanvas.DrawImage(BmpInput, 0, 0, face.rect, GraphicsUnit.Pixel); //grayscale algıla

                    FaceCanvas.DrawImage(ImageFrame.ToBitmap(), 0, 0, face.rect, GraphicsUnit.Pixel); //renkli algıla

                    ExtFaces[faceNo] = ExtractedFace;
                    faceNo++;

                }

                faceNo = 0;


            }
            else
            {
                //lblBilgi.Text = "Bilgi: Yüz algılanamadı.";
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////
            //renk algılama
            Bitmap bMap = ExtFaces[0]; //örnek olarak ilk algılanan yüzü hesaba katıyor ortalama alırken

         Color c=   getDominantColor(bMap);

         //string[] colors = { c.R.ToString(), c.B.ToString(), c.G.ToString()};
         string colors = c.R.ToString() + "," + c.B.ToString() + "," + c.G.ToString();
         return colors;
        }


        /*
        private void ProcessFrame(object sender, EventArgs arg)
        {
            //fetch the frame captured by web camera
            ImageFrame = capture.QueryFrame();

            //show the image in the EmguCV ImageBox
            //CamImageBox.Image = ImageFrame;
        }


        private void CameraCapture_Load(object sender, EventArgs e)
        {
            //path xml
            haar = new HaarCascade("haarcascade_frontalface_alt_tree.xml"); 
        }

        
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            
            lblBilgi.Text = "Bilgi: ";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Image InputImg = Image.FromFile(openFileDialog1.FileName);
                ImageFrame = new Image<Bgr, byte>(new Bitmap(InputImg));
                //CamImageBox.Image = ImageFrame;
                
            }             
        }
        */

        /*
        private void button1_Click(object sender, EventArgs e)
        {
            //algılama butonuna basınca algılamaya başla
            //DetectFaces();
        }

        
        private void btnRenk_Click(object sender, EventArgs e)
        {
            //Bitmap bMap = ExtFaces[0]; //örnek olarak ilk algılanan yüzü hesaba katıyor ortalama alırken

            //getDominantColor(bMap);
        } */


        




    }
}
