﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1
{
    /// <summary>
    /// Summary description for Handler1
    /// </summary>
    public class Handler1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            JemiWS.MyWebSSoapClient ws = new JemiWS.MyWebSSoapClient();

            //instantiate your webservice

            //invoke GetImage method passing file name as param

            string fileName = context.Request["fileName"];           
            byte[] binImage = ws.GetImageFile(fileName);
            
            if (binImage.Length == 1)
            {
                //File Not found by the web services
                //Maybe you want to display an alternate image here...
            }
            else
            {
                //file found, return it
                context.Response.ContentType = "image/jpeg";
                context.Response.BinaryWrite(binImage);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}