﻿using System;
using SQLite.Net.Attributes;

namespace MobileClothingAdvisor
{
	public class Clothes
	{
		[PrimaryKey, AutoIncrement]
		public int _Clothingid { get; set; }
		public int _Categoryid { get; set; }
		public int Userid { get; set; }
		public string Color { get; set; }
	}
}

