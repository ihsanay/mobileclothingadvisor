﻿using System;
using SQLite.Net.Attributes;

namespace MobileClothingAdvisor
{
	public class User
	{ //Name, Surname, Username, Password, Height, Weight, Gender, PhotoID
		[PrimaryKey, AutoIncrement]
		public int _id { get; set; }
		public string Name { get; set; }
		public string Surname { get; set; }
		public string Password { get; set; }
		public string Weight { get; set; }
		public string Gender { get; set; }
		public string SkinColor { get; set; }
		//public int PhotoID { get; set; }



	}
}

