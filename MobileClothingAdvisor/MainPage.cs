﻿using System;

using Xamarin.Forms;

namespace MobileClothingAdvisor
{
	public class MainPage : ContentPage
	{
		private MyWebSSoapClient ws = new MyWebSSoapClient();
		private TempConvertSoapClient wsTemp = new TempConvertSoapClient();


		public MainPage ()
		{
			var lblWelcome = new Label 
			{
				Text = "Hoşgeldiniz",
				WidthRequest = 150
			};

			Label lblGiris = new Label 
			{
				Text = "Giriş:"
			};

			var entryText = new Entry 
			{
				//place holder, açıklama gibi gri yazan kısım.
				Placeholder = "Enter your name",
				WidthRequest = 150,
				HeightRequest = 50
			};

			var entryPassword = new Entry 
			{
				//place holder, açıklama gibi gri yazan kısım.
				Placeholder = "Enter your password",
				IsPassword = true,
				WidthRequest = 150,
				HeightRequest = 50
			};

			Button btnLogin = new Button {
				Text = "Giriş"
			};

			Button btnKayitOl = new Button {
				Text = "Kayıt Ol"
			};
					
			btnLogin.Clicked += (sender, e) => 
			{
				//lblWelcome.Text = entryText.Text;
				lblWelcome.Text = "Clicked the button";

				Navigation.PushModalAsync(new TabPage(entryText.Text));	 //entryText teki değeri tabPage e gönderiyor.
			};

			btnKayitOl.Clicked += (sender, e) => 
			{
				Navigation.PushModalAsync(new RegisterPage());
				
			};

			Button btnOneriAl = new Button 
			{
				Text = "Öneri Al"
			};
			btnOneriAl.Clicked += (sender, e) => 
			{
				//lblWelcome.Text = entryText.Text;
				Navigation.PushModalAsync(new OneriAl());	 //entryText teki değeri tabPage e gönderiyor.
			};


			Button btnMasterPage = new Button 
			{
				Text = "MasterPage Ac"
			};
			btnMasterPage.Clicked += (sender, e) => 
			{
				//lblWelcome.Text = entryText.Text;
				lblWelcome.Text = wsTemp.FahrenheitToCelsius("70");
			};


			Button btnWebServis = new Button 
			{
				Text = "WebServisDeneme"
			};
			btnWebServis.Clicked += (sender, e) => 
			{
				//lblWelcome.Text = entryText.Text;
				lblWelcome.Text = wsTemp.FahrenheitToCelsius("70");
			};

			Content = new StackLayout { 
				Padding = 20,
				Children = {
					//new Label { Text = "Hello ContentPage" }
					lblWelcome,
					lblGiris,
					entryText,
					entryPassword,
					btnLogin,
					btnKayitOl,
//					btnOneriAl,
//					btnMasterPage,
//					btnWebServis
				},
				Spacing = 9,
			};
		}
	}
}


