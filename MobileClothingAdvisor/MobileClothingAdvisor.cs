﻿using System;

using Xamarin.Forms;

namespace MobileClothingAdvisor
{
	public class App : Application
	{
		public App ()
		{
			//bu bize anasayfanın Mainpage.cs de yazdığımızı gösteriyor.
			MainPage = new MainPage ();
			//MainMaster = new MasterDetailPageNavigation.MainPage();

			//MainPage = new MobileClothingAdvisor.MainMaster();
			// The root page of your application
			/*
			MainPage = new ContentPage {
				Content = new StackLayout {
					VerticalOptions = LayoutOptions.Center,
					Children = {
						new Label {
							XAlign = TextAlignment.Center,
							Text = "Welcome to Xamarin Forms!"
						}
					}
				}
			};*/
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

