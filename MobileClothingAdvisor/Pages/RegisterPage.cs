﻿using System;

using Xamarin.Forms;
using XLabs.Forms.Controls;

namespace MobileClothingAdvisor
{


	//[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
	public class RegisterPage : ContentPage
	{
		public RegisterPage ()
		{
			

			Label lblAd = new Label 
			{
				Text = "Ad:"
			};

			Entry txtAd = new Entry
			{
				Placeholder = "Adınız...",
				WidthRequest = 150,
				HeightRequest = 50
			};

			Label lblSoyad = new Label 
			{
				Text = "Soyad:"
			};

			Entry txtSoyad = new Entry
			{
				Placeholder = "Soyadınız...",
				WidthRequest = 150,
				HeightRequest = 50
			};

			Label lblPassword = new Label 
			{
				Text = "Şifre:"
			};

			Entry txtPassword = new Entry
			{
				Placeholder = "Parolanız (Tekrar)...",
				WidthRequest = 150,
				HeightRequest = 50,
				IsPassword = true
			};

			Label lblRePassword = new Label 
			{
				Text = "Şifre:"
			};

			Entry txtRePassword = new Entry
			{
				Placeholder = "Parolanız (Tekrar)...",
				WidthRequest = 150,
				HeightRequest = 50,
				IsPassword = true
			};

			Label lblHeight = new Label 
			{
				Text = "Boy:"
			};

			Entry txtHeight = new Entry
			{
				Placeholder = "Örn: 170",
				WidthRequest = 150,
				HeightRequest = 50,

							
			};

			Label lblWeight = new Label 
			{
				Text = "Kilo:"
			};

			EntryCell txtCellWeight = new EntryCell
			{
				Label = "Kilo:",
				Placeholder = "Kilonuz",
				Text = "deneme"
			};


			//------------------------


			Label header = new Label
			{
				Text = "Kayıt",
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(EntryCell)),
				HorizontalOptions = LayoutOptions.Center
			};

			TableView tableView = new TableView
			{
				Intent = TableIntent.Form,
				Root = new TableRoot
				{
					new TableSection
					{
						new EntryCell
						{
							Label = "Adınız:",
							Placeholder = "Adınızı girin."
						},
						new EntryCell
						{
							Label = "Soyadınız:",
							Placeholder = "Soyadınızı girin."
						},

						new EntryCell
						{
							Label = "K.Adınız:",
							Placeholder = "Kullanıcı Adınızı Girin."
						},
						/*
						new EntryCell
						{
							Label = "Şifreniz:",
							Placeholder = "Şifrenizi Girin.",

						},


						new ViewCell
						{
							View = new StackHorizontal
							{
								Orientation = StackOrientation.Horizontal,
								Spacing=0,

								Children = {
									new Label
									{
										Text = "Password",
										HorizontalOptions = LayoutOptions.FillAndExpand
									},
									txtPassword

								}
							}
						},
						*/



						new EntryCell
						{
							Label = "Boyunuz (cm):",
							Placeholder = "Boyunuzu Girin. (cm)",
							Keyboard = Keyboard.Numeric					
						},

						new EntryCell
						{
							Label = "Kilonuz:",
							Placeholder = "Kilonuzu Girin.",
							Keyboard = Keyboard.Numeric
						},

						new EntryCell
						{							
							Label = "Cinsiyet (E/K):",
							Placeholder = "E / K"							
						}
					}
				}
			};

			Button btnKaydet = new Button {
				Text = "Kaydet"
			};

			btnKaydet.Clicked += (sender, e) => 
			{
				

				DisplayAlert ("Mesaj", "Kaydedildi", "OK");


				//lblWelcome.Text = entryText.Text;
				//lblWelcome.Text = "Clicked the button";

				//Navigation.PushModalAsync(new TabPage(entryText.Text));	 //entryText teki değeri tabPage e gönderiyor.
			};

			// Accomodate iPhone status bar.
			//this.Padding = new Thickness(10, Device.OnPlatform(20, 0, 0), 10, 5);

			Content = new StackLayout { 
				Children = {
					/*

					lblAd,
					txtAd,
					lblSoyad,
					txtSoyad,
					lblPassword,
					txtPassword,
					lblRePassword,
					txtRePassword,
					lblHeight,
					txtHeight,
					lblWeight,
					*/
					header,
					tableView,
					btnKaydet


					//new Label { Text = "Hello ContentPage" }
				}
			};
		}

	}
}


