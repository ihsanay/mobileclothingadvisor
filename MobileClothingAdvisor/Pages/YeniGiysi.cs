﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using XLabs.Forms.Controls;

//using Media.Plugin;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Media;

using System.IO;

using System.Runtime.Serialization;

namespace MobileClothingAdvisor
{
	public class YeniGiysi : ContentPage
	{
		private MyWebSSoapClient ws = new MyWebSSoapClient();
		private TempConvertSoapClient wsTemp = new TempConvertSoapClient();


		public YeniGiysi ()
		{

			Title = "Giysilerim";

			Button btnTakePhoto = new Button {
				Text = "Yeni Giysi Çek"
			};

			Button btnPickPhoto = new Button {
				Text = "Galeriden Seç"
			};


			Image image = new Image{ };

			btnTakePhoto.Clicked += async (sender, args) =>
			{

				if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
				{
					DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
					return;
				}

				var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
					{

						Directory = "Sample",
						Name = "test.jpg"
					});

				if (file == null)
					return;

				DisplayAlert("File Location", file.Path, "OK");

				image.Source = ImageSource.FromStream(() =>
					{
						var stream = file.GetStream();
						file.Dispose();
						return stream;
					});

				/*
				using (var streamReader = new StreamReader(image))
				{
					var bytes = default(byte[]);
					using (var memstream = new MemoryStream())
					{
						streamReader.BaseStream.CopyTo(memstream);
						bytes = memstream.ToArray();


					}
				}
				*/

				//lblWebServisBilgi.Text = MyWebSSoapClient.add(5,6).ToString();
				//lblWebServisBilgi.Text = ws.add(5,6).ToString();

			};


			//			webServisButton.Clicked += (object sender, EventArgs e) => 
			//			{
			//				//lblWebServisBilgi.Text = ws.add(5,6).ToString();
			//				//int sum = ws.add(5,6);
			//				//lblWebServisBilgi.Text = (5+6).ToString();
			//				//lblWebServisBilgi.Text = wsTemp.FahrenheitToCelsius("70");
			//				lblWebServisBilgi.Text = ws.add(5, 4).ToString();
			//			};




			btnPickPhoto.Clicked += async (sender, args) =>
			{
				if (!CrossMedia.Current.IsPickPhotoSupported)
				{
					DisplayAlert("Photos Not Supported", ":( Permission not granted to photos.", "OK");
					return;
				}
				var file = await CrossMedia.Current.PickPhotoAsync();


				if (file == null)
					return;

				image.Source = ImageSource.FromStream(() =>
					{
						var stream = file.GetStream();
						file.Dispose();
						return stream;
					});


				//MobileClothingAdvisor _Service = new MobileClothingAdvisor();
				//int a = _Service.add(5,4);
				//lblWebServisBilgi.Text = a.ToString();

				//ServiceClassName _Service = new ServiceClassName();
			};


			Content = new StackLayout { 
				Children = {

					btnTakePhoto,
					btnPickPhoto,
					image
					//new Label { Text = "Hello ContentPage" }
				}
			};
		}
	}
}


