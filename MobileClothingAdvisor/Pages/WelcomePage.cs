﻿using System;

using Xamarin.Forms;

namespace MobileClothingAdvisor
{
	public class WelcomePage : ContentPage
	{
		public WelcomePage (string userName)
		{
			Title = "Ana Menü";

			Button btnOneriAl = new Button {
				Text = "Öneri Al"
			};

			btnOneriAl.Clicked += (sender, e) => 
			{				
				//DisplayAlert ("Mesaj", "Butona Basıldı.", "OK");			
				//lblWelcome.Text = entryText.Text;
				//lblWelcome.Text = "Clicked the button";

				Navigation.PushModalAsync(new OneriAl());	 //entryText teki değeri tabPage e gönderiyor.
			};

			Button btnGiysilerim = new Button {
				Text = "Giysilerim"
			};

			btnGiysilerim.Clicked += (sender, e) => 
			{				
				//DisplayAlert ("Mesaj", "Butona Basıldı.", "OK");			
				//lblWelcome.Text = entryText.Text;
				//lblWelcome.Text = "Clicked the button";

				Navigation.PushModalAsync(new Giysilerim());	 //entryText teki değeri tabPage e gönderiyor.
			};

			Content = new StackLayout {
				//VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Center,
				Children = {
					new Label { Text = "hoş geldiniz: " + userName },
					btnOneriAl,
					btnGiysilerim,

					//new Label { Text = userName }
				}
			};
		}
	}
}


