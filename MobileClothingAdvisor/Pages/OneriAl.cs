﻿using System;

using Xamarin.Forms;

using XLabs.Forms;
using XLabs.Forms.Controls;

namespace MobileClothingAdvisor
{
	public class OneriAl : ContentPage
	{
		public OneriAl ()
		{
			Title = "Öneri Al";

			Label lblUst = new Label 
			{
				Text = "Üst:",
				FontAttributes = FontAttributes.Italic
			};

			Label lblTip = new Label 
			{
				Text = "Tip Önerileri:",
				FontAttributes = FontAttributes.Italic
			};

			var lwUst = new ListView ();
			lwUst.ItemsSource = new string[] {
				"Gömlek",
				"Kazak"
			};

			Label lblUstRenk = new Label 
			{
				Text = "Renk:",
				FontAttributes = FontAttributes.Italic
			};

			var lwUstRenk = new ListView ();
			lwUstRenk.ItemsSource = new string[] {
				"Kırmızı",
				"Yeşil"
			};

			Label lblAlt = new Label 
			{
				Text = "Alt:",
				FontAttributes = FontAttributes.Italic
			};



			var lwAlt = new ListView ();
			lwAlt.ItemsSource = new string[] {
				"Kot Pantolon",
				"Keten Pantolon"
			};

			Label lblAltRenk = new Label 
			{
				Text = "Renk:",
				FontAttributes = FontAttributes.Italic
			};
			var lwAltRenk = new ListView ();
			lwAltRenk.ItemsSource = new string[] {
				"Lacivert",
				"Siyah"
			};

			//chk gözükmüyor.
			CheckBox chk=new CheckBox()
			{
				Checked = false
			};

			Content = new StackLayout {
				Children = {
					lblUst,
					lblTip,
					lwUst,
					lblUstRenk,
					lwUstRenk,
					lblAlt,
					lwAlt,
					lblAltRenk,
					lwAltRenk
					//new Label { Text = "Hello ContentPage" }
				}
			};
		}
	}
}


