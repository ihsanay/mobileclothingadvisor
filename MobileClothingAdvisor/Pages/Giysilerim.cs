﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using XLabs.Forms.Controls;

//using Media.Plugin;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Media;

using System.IO;

using System.Runtime.Serialization;

namespace MobileClothingAdvisor
{
	public class Giysilerim : ContentPage
	{
		private MyWebSSoapClient ws = new MyWebSSoapClient();
		private TempConvertSoapClient wsTemp = new TempConvertSoapClient();


		public Giysilerim ()
		{

			Title = "Giysilerim";

			Button btnYeniGiysi = new Button {
				Text = "Yeni Giysi"
			};

			btnYeniGiysi.Clicked += (sender, e) => 
			{
				Navigation.PushModalAsync(new YeniGiysi());

			};


			Image image = new Image{ };



			Content = new StackLayout { 
				Children = {

					btnYeniGiysi,
					new Label { Text = "Galerinizde Giysi Bulunmamaktadır." }
				}
			};
		}
	}
}


