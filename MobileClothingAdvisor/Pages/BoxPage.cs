﻿using System;

using Xamarin.Forms;

namespace MobileClothingAdvisor
{
	public class BoxPage : ContentPage
	{

		Color[] colors = new Color[]{Color.Blue, Color.Black, Color.White, Color.Yellow, Color.Aqua, Color.Navy, Color.Red, Color.Maroon};
		Random r = new Random();


		public BoxPage ()
		{
			Title = "Renk";

			Button btnRenk = new Button {
				Text = "Renk Değiştir",
				WidthRequest = 150,
				HeightRequest = 50,
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Center
			};



			btnRenk.Clicked += BtnRenk_Clicked;


			Content = new StackLayout { 
				Children = {
					btnRenk
					//new Label { Text = "Hello ContentPage" }
				}
			};
		}

		void BtnRenk_Clicked (object sender, EventArgs e)
		{
			var index = r.Next (0, colors.Length - 1);
			BackgroundColor = colors[index];
		}
	}
}


